package com.bibao.code;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestHello {

	@Test
	public void testGreeting() {
		Hello hello = new Hello();
		assertEquals("Hello Alice", hello.greeting("Alice")); 
	}

}
